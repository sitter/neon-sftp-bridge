# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2020 Harald Sitter <sitter@kde.org>

deploy: clean neon-sftp-bridge
	./deploy.sh

test:
	go test -v ./...

neon-sftp-bridge:
	go build -o $@ -v

clean:
	rm -f neon-sftp-bridge

.PHONY: deploy test clean
