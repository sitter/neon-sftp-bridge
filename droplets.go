// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

package main

import (
	"context"

	"github.com/digitalocean/godo"
	"golang.org/x/oauth2"
)

type tokenSource struct {
	AccessToken string
}

func (t *tokenSource) Token() (*oauth2.Token, error) {
	token := &oauth2.Token{
		AccessToken: t.AccessToken,
	}
	return token, nil
}

func listAddresses(ctx context.Context, client *godo.Client) ([]string, error) {
	addresses := []string{}

	// api docs say maximum is 200 per page, so get that. We'll want as few
	// requests as possible so as to not hit the api rate limit.
	opt := &godo.ListOptions{PerPage: 200}
	for {
		droplets, resp, err := client.Droplets.List(ctx, opt)
		if err != nil {
			return nil, err
		}

		for _, d := range droplets {
			for _, network := range d.Networks.V4 {
				addresses = append(addresses, network.IPAddress)
			}
			for _, network := range d.Networks.V6 {
				addresses = append(addresses, network.IPAddress)
			}
		}

		// last page
		if resp.Links == nil || resp.Links.IsLastPage() {
			break
		}

		page, err := resp.Links.CurrentPage()
		if err != nil {
			return nil, err
		}

		opt.Page = page + 1
	}

	return addresses, nil
}

func dropletsPoll() ([]string, error) {
	// FIXME: needs rate limiter

	addresses := []string{}

	for _, accessToken := range conf.DOAccessTokens {
		token := &tokenSource{
			AccessToken: accessToken,
		}

		oauthClient := oauth2.NewClient(context.Background(), token)
		client := godo.NewClient(oauthClient)

		ctx := context.TODO()

		list, err := listAddresses(ctx, client)
		if err != nil {
			return addresses, err
		}

		addresses = append(addresses, list...)
	}

	return addresses, nil
}
