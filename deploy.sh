#!/bin/bash
# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: 2018 Bhushan Shah <bshah@kde.org>
# SPDX-FileCopyrightText: 2018-2021 Harald Sitter <sitter@kde.org>

# script to deploy the sftp-bridge to its server

set -ex

USER="neonsftpproxy"
REMOTE="charlotte.kde.org"
REMOTE_HOME="/home/$USER"
REMOTE_DATA="$REMOTE_HOME/data"
REMOTE_BIN="$REMOTE_HOME/bin"
REMOTE_SYSTEMD="$REMOTE_HOME/.config/systemd/user"

ssh="ssh $USER@$REMOTE --"

echo -n "Making directories ... "
$ssh mkdir -p -m 700 $REMOTE_DATA
$ssh mkdir -p -m 700 $REMOTE_BIN
$ssh mkdir -p -m 700 $REMOTE_SYSTEMD
echo "[done]"

echo "Starting installation of the neon-sftp-bridge ... "
rsync -avz --progress -e ssh neon-sftp-bridge "$USER@$REMOTE:$REMOTE_BIN"
rsync -avz --progress -e ssh systemd/ "$USER@$REMOTE:$REMOTE_SYSTEMD"
echo "[done]"

echo -n "Installing systemd service files ... "
$ssh systemctl --user daemon-reload
$ssh systemctl --user enable neon-sftp-bridge.service
$ssh systemctl --user enable neon-sftp-bridge.socket
# Sockets cannot be stopped while the service is running. Since we'll want to
# have zero downtime we do not force restart the socket but instead stop the
# service. What happens then is that our service refuses new connections but
# still works off pending requests. Once it is down or a timeout is hit the
# service actually stops. Meanwhile the socket queues new requests. Once
# the service is stopped the socket starts a new instance of the service and
# feeds it the queued requests. Thereby we can have zero downtime so long
# as the socket itself doesn't need restarting.
$ssh systemctl --user start neon-sftp-bridge.socket || true
$ssh systemctl --user stop neon-sftp-bridge.service || true
echo "[done]"
