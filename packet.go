// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

package main

import (
	"log"
	"os"

	"github.com/packethost/packngo"
)

func packetsList(c *packngo.Client) ([]string, error) {
	addresses := []string{}

	projects, _, err := c.Projects.List(nil)
	if err != nil {
		return addresses, err
	}

	for _, project := range projects {
		listOpt := &packngo.ListOptions{}
		devices, _, err := c.Devices.List(project.ID, listOpt)
		if err != nil {
			return addresses, err
		}
		for _, device := range devices {
			for _, network := range device.Network {
				addresses = append(addresses, network.Address)
			}
		}
	}

	return addresses, nil
}

func packetsPoll() ([]string, error) {
	// WARNING: packngo is not reentrant!!!! it gets the token from the env
	//   exclusively, so new clients must only be made serially

	addresses := []string{}
	for _, token := range conf.PacketAccessTokens {

		err := os.Setenv("PACKET_AUTH_TOKEN", token)
		if err != nil {
			log.Fatal(err)
		}

		c, err := packngo.NewClient()
		if err != nil {
			log.Fatal(err)
		}

		err = os.Unsetenv("PACKET_AUTH_TOKEN")
		if err != nil {
			log.Fatal(err)
		}

		list, err := packetsList(c)
		if err != nil {
			return addresses, err
		}

		addresses = append(addresses, list...)
	}

	return addresses, nil
}
