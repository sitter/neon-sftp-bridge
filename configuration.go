// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

package main

import (
	"io/ioutil"
	"os"
	"path/filepath"

	yaml "gopkg.in/yaml.v2"
)

var conf = loadConfiguration()

type doYamlConfiguration struct {
	// This is the primary access token. It exists so the marshal rule is
	// compatible with the legacy config which simply has one access token for
	// the cloud team.
	AccessToken string `yaml:"access_token"`
	// This is a list of extra tokens, it may not be present in the yaml or
	// be empty. The token for the persistent servers team goes here.
	ExtraAccessTokens []string `yaml:"extra_access_tokens"`
}

func loadDigitalOceanConf() *doYamlConfiguration {
	path := filepath.Join(os.Getenv("HOME"), ".config/pangea-digital-ocean.yaml")
	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		// FIXME: should probably pass and allow everything, but log warning
		panic("missing .config/pangea-digital-ocean.yaml")
	}

	y := &doYamlConfiguration{}
	err = yaml.Unmarshal(yamlFile, y)
	if err != nil {
		panic(err)
	}

	return y
}

type packetYamlConfiguration struct {
	AccessTokens []string `yaml:"access_tokens"`
}

func loadPacketConf() *packetYamlConfiguration {
	path := filepath.Join(os.Getenv("HOME"), ".config/pangea-packet.yaml")
	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		// FIXME: should probably pass and allow everything, but log warning
		panic("missing .config/pangea-packet.yaml")
	}

	y := &packetYamlConfiguration{}
	err = yaml.Unmarshal(yamlFile, y)
	if err != nil {
		panic(err)
	}

	return y
}

type configuration struct {
	DOAccessTokens     []string
	PacketAccessTokens []string
}

func loadConfiguration() *configuration {
	do := loadDigitalOceanConf()
	packet := loadPacketConf()

	c := &configuration{}
	c.DOAccessTokens = append(c.DOAccessTokens, do.AccessToken)
	c.DOAccessTokens = append(c.DOAccessTokens, do.ExtraAccessTokens...)
	c.PacketAccessTokens = packet.AccessTokens

	return c
}
